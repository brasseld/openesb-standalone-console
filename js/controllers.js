'use strict';

/* Controllers */

var host = 'http://localhost:4848/openesb';

var mainController = angular.module('mainController', []);

mainController.controller('AppCtrl', 
	function($scope, $route, $http, $q) {
		
		$http.get(host+'/').success(function(data) {
    		$scope.informations = data;
        });

		// Listen for changes to the Route. When the route
		// changes, let's set the renderAction model value so
		// that it can render in the Strong element.
		$scope.$on(
			"$routeChangeSuccess",
			function(){
				
				// Update the rendering.
                $scope.assemblies = $http.get(host+'/assemblies', {cache: true});
			    $scope.components = $http.get(host+'/components', {cache: true});
			    $scope.libraries = $http.get(host+'/libraries', {cache: true});
			    $scope.endpoints = $http.get(host+'/nmr', {cache: true});

			    if ($route.current.originalPath != undefined) {
				    $scope.isAssemblies = ($route.current.originalPath.indexOf("/assemblies") == 0);
					$scope.isComponents = ($route.current.originalPath.indexOf("/components") == 0);
					$scope.isLibraries = ($route.current.originalPath.indexOf("/libraries") == 0);
					$scope.isEndpoints = ($route.current.originalPath.indexOf("/endpoints") == 0);
				}

			    $q.all([$scope.assemblies, $scope.components, $scope.libraries, $scope.endpoints]).then(function(values) {
			    	$scope.assembliesCount = values[0].data.serviceAssembly.length;
			    	$scope.componentsCount = values[1].data.component.length;
			    	$scope.librariesCount = values[2].data.sharedLibrary.length;
			    	$scope.endpointsCount = values[3].data.endpointCount;
				});
			}
		);
	}
);

var assembliesController = angular.module('assembliesController', []);

assembliesController.controller('AssembliesListCtrl', ['$scope', '$http',
	function AssembliesListCtrl($scope, $http) {
    	$http.get(host+'/assemblies').success(function(data) {
    		$scope.assemblies = data.serviceAssembly;
        });
        $scope.predicate = 'name';
	}
]);

assembliesController.controller('AssemblyDetailCtrl', ['$scope', '$route', '$http', '$routeParams',
	function AssemblyDetailCtrl($scope, $route, $http, $routeParams) {
		$scope.assemblyName = $routeParams.assemblyName;

	    $scope.isGeneralTab = ($route.current.originalPath.lastIndexOf("/general") != -1);
		$scope.isDescriptorTab = ($route.current.originalPath.lastIndexOf("/descriptor") != -1);

	    if ($scope.isDescriptorTab) {
	    	$http.get(host+'/assemblies/' + $scope.assemblyName + "/descriptor").success(
				function(data) {
					$scope.descriptor = data;
					$scope.tabcontent = "partials/assemblies/assembly-descriptor.html";

	    		});
	    } else {
	    	// Default behaviour : general tab
	    	$scope.isGeneralTab = true;

	    	$http.get(host+'/assemblies/'+$scope.assemblyName).success(
				function(data) {
					$scope.assembly = data;
					$scope.tabcontent = "partials/assemblies/assembly-general.html";

	    		});
	    }
	}
]);

var componentsController = angular.module('componentsController', []);

componentsController.controller('ComponentsListCtrl', ['$scope', '$http',
	function ComponentsListCtrl($scope, $http) {
    	$http.get(host+'/components').success(function(data) {
    		$scope.components = data.component;
        });
        $scope.predicate = 'name';
	}
]);

componentsController.controller('ComponentDetailCtrl', ['$scope', '$route', '$http', '$routeParams',
	function ComponentDetailCtrl($scope, $route, $http, $routeParams) {
		$scope.componentName = $routeParams.componentName;

	    $scope.isGeneralTab = ($route.current.originalPath.lastIndexOf("/general") != -1);
		$scope.isDescriptorTab = ($route.current.originalPath.lastIndexOf("/descriptor") != -1);
		$scope.isLibrariesTab = ($route.current.originalPath.lastIndexOf("/libraries") != -1);

	    if ($scope.isDescriptorTab) {
	    	$http.get(host+'/components/' + $scope.componentName + "/descriptor").success(
				function(data) {
					$scope.descriptor = data;
					$scope.tabcontent = "partials/components/component-descriptor.html";

	    		});
	    } else if ($scope.isLibrariesTab) {
	    	$http.get(host+'/components/' + $scope.componentName).success(
				function(data) {
					$scope.libraries = data.sharedLibraries;
					$scope.tabcontent = "partials/components/component-libraries.html";

	    		});
	    } else {
	    	// Default behaviour : general tab
	    	$scope.isGeneralTab = true;

	    	$http.get(host+'/components/'+$scope.componentName).success(
				function(data) {
					$scope.component = data;
					$scope.tabcontent = "partials/components/component-general.html";
	    		});
	    }
	}
]);

var librariesController = angular.module('librariesController', []);

librariesController.controller('LibrariesListCtrl', ['$scope', '$http',
	function LibrariesListCtrl($scope, $http) {
    	$http.get(host+'/libraries').success(function(data) {
    		$scope.libraries = data.sharedLibrary;
        });
        $scope.predicate = 'name';
	}
]);

librariesController.controller('LibraryDetailCtrl', ['$scope', '$route', '$http', '$routeParams',
	function LibraryDetailCtrl($scope, $route, $http, $routeParams) {
		$scope.libraryName = $routeParams.libraryName;
		
	    $scope.isGeneralTab = ($route.current.originalPath.lastIndexOf("/general") != -1);
		$scope.isDescriptorTab = ($route.current.originalPath.lastIndexOf("/descriptor") != -1);

		if ($scope.isDescriptorTab) {
	    	$http.get(host+'/libraries/' + $scope.libraryName + "/descriptor").success(
				function(data) {
					$scope.descriptor = data;
					$scope.tabcontent = "partials/libraries/library-descriptor.html";

	    		});
	    } else {
	    	// Default behaviour : general tab
	    	$scope.isGeneralTab = true;

	    	$http.get(host+'/libraries/'+$scope.libraryName).success(
				function(data) {
					$scope.library = data;
					$scope.tabcontent = "partials/libraries/library-general.html";

	    		});
	    }
	}
]);

var endpointsController = angular.module('endpointsController', []);

endpointsController.controller('EndpointsListCtrl', ['$scope', '$http',
	function EndpointsListCtrl($scope, $http) {
    	$http.get(host+'/nmr/endpoints').success(function(data) {
    		$scope.endpoints = data;
        });
        $scope.predicate = 'name';
	}
]);