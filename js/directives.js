'use strict';

/* Directives */

angular.module('directives', [])
	.directive('prettyprint', function() {
	    return {
	        restrict: 'C',
	        link: function postLink(scope, element, attrs) {
	              element.html(prettyPrintOne(scope.descriptor));
	        }
	    };
	})
	.directive('jbiActions', function() {
	    return {
	    	scope: {
		      item: '=component'
		    },
	        restrict: 'E',
	        replace: true,
		      controller: function($scope) {
		        
		        $scope.stop = function() {
				    alert('stop :' + $scope.item.name);
				}

				$scope.start = function() {
				    alert('start :' + $scope.item.name);
				}

				$scope.shutdown = function() {
				    alert('shutdown :' + $scope.item.name);
				}
		      },
	        templateUrl: 'partials/directives/jbi-actions.html'
	    };
	})
	.directive('jbiState', function() {
	    return {
	    	scope: {
		      item: '=component'
		    },
	        restrict: 'E',
	        templateUrl: 'partials/directives/jbi-state.html'
	    };
	});