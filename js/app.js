'use strict';

/* App Module */

var consoleApp = angular.module('consoleApp', [
  'ngRoute',
  'directives',
  'mainController',
  'assembliesController',
  'componentsController',
  'librariesController',
  'endpointsController'
]);

consoleApp.config(['$routeProvider',
  function($routeProvider) {
    $routeProvider.
      when('/',{
        templateUrl: 'partials/home.html',
        controller: 'AppCtrl'
      }).

      when('/assemblies',{
    	  templateUrl: 'partials/assemblies/assemblies-list.html',
    	  controller: 'AssembliesListCtrl'
      }).

      when('/assemblies/:assemblyName',{
        templateUrl: 'partials/assemblies/assembly-detail.html',
        controller: 'AssemblyDetailCtrl'
      }).

      when('/assemblies/:assemblyName/general',{
        templateUrl: 'partials/assemblies/assembly-detail.html',
        controller: 'AssemblyDetailCtrl'
      }).

      when('/assemblies/:assemblyName/descriptor',{
        templateUrl: 'partials/assemblies/assembly-detail.html',
        controller: 'AssemblyDetailCtrl'
      }).

      when('/components',{
        templateUrl: 'partials/components/components-list.html',
        controller: 'ComponentsListCtrl'
      }).

      when('/components/:componentName',{
        templateUrl: 'partials/components/component-detail.html',
        controller: 'ComponentDetailCtrl'
      }).

      when('/components/:componentName/general',{
        templateUrl: 'partials/components/component-detail.html',
        controller: 'ComponentDetailCtrl'
      }).

      when('/components/:componentName/descriptor',{
        templateUrl: 'partials/components/component-detail.html',
        controller: 'ComponentDetailCtrl'
      }).

      when('/components/:componentName/libraries',{
        templateUrl: 'partials/components/component-detail.html',
        controller: 'ComponentDetailCtrl'
      }).

      when('/libraries',{
        templateUrl: 'partials/libraries/libraries-list.html',
        controller: 'LibrariesListCtrl'
      }).

      when('/libraries/:libraryName',{
        templateUrl: 'partials/libraries/library-detail.html',
        controller: 'LibraryDetailCtrl'
      }).

      when('/libraries/:libraryName/general',{
        templateUrl: 'partials/libraries/library-detail.html',
        controller: 'LibraryDetailCtrl'
      }).

      when('/libraries/:libraryName/descriptor',{
        templateUrl: 'partials/libraries/library-detail.html',
        controller: 'LibraryDetailCtrl'
      }).

      when('/endpoints',{
        templateUrl: 'partials/endpoints/endpoints-list.html',
        controller: 'EndpointsListCtrl'
      }).
      
      otherwise({
        redirectTo: '/'
      });
  }]);